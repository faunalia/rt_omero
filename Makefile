UI_SOURCES=$(wildcard ui/*.ui)
UI_FILES=$(patsubst %.ui,%_ui.py,$(UI_SOURCES))

GEN_FILES = resources.py ${UI_FILES}
PLUGINNAME = rt_omero

all: $(GEN_FILES)


$(UI_FILES): %_ui.py: %.ui
	pyuic4 -o $@ $<
	
resources.py: resources.qrc
	pyrcc4 -o resources.py resources.qrc


clean:
	rm -f $(GEN_FILES) *.pyc

#package:
#	cd .. && rm -f rt_omero.zip && zip -r rt_omero.zip rt_omero -x \*.svn* -x \*.pyc -x \*~ -x \*entries\* -x \*.git\* -x \*docs/cache -x #\*docs/cache\* -x \*docs/*.db3\* -x \*.omero-cache\*

package:
	# Create a zip package of the plugin named $(PLUGINNAME).zip.
	# This requires use of git (your plugin development directory must be a
	# git repository).
	# To use, pass a valid commit or tag as follows:
	#   make package VERSION=Version_0.3.2
	@echo
	@echo "------------------------------------"
	@echo "Exporting plugin to zip package.	"
	@echo "------------------------------------"
	rm -f $(PLUGINNAME).zip
	git archive --prefix=$(PLUGINNAME)/ -o $(PLUGINNAME).zip $(VERSION)
	echo "Created package: $(PLUGINNAME).zip"


